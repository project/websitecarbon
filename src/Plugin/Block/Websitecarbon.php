<?php

namespace Drupal\websitecarbon\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Provides a Websitecarbon block.
 *
 * @Block(
 *   id = "websitecarbon_block",
 *   admin_label = @Translation("Websitecarbon block"),
 * )
 */
class Websitecarbon extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The logger factory.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The API Url.
   *
   * @var string
   */
  protected $apiUrl = "https://api.websitecarbon.com/b?url=";

  /**
   * Constructs a new Websitecarbon object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   A guzzle http client instance.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   A guzzle http client instance.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger Factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $httpClient, RequestStack $requestStack, LoggerChannelFactoryInterface $loggerFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $httpClient;
    $this->requestStack = $requestStack;
    $this->loggerFactory = $loggerFactory->get('websitecarbon');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('request_stack'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['mode'] = [
      '#type' => 'select',
      '#title' => $this->t('API call mode'),
      '#description' => $this->t('Choose the API call mode for this block.'),
      '#default_value' => $config['mode'] ?? 'php',
      '#required' => TRUE,
      '#options' => [
        'php' => 'PHP',
        'js' => 'Javascript',
      ],
    ];

    $form['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme'),
      '#description' => $this->t('Choose the theme for this block.'),
      '#default_value' => $config['theme'] ?? 'dark',
      '#required' => TRUE,
      '#options' => [
        'dark' => 'Dark',
        'light' => 'Light',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('mode', $form_state->getValue('mode'));
    $this->setConfigurationValue('theme', $form_state->getValue('theme'));
    $this->messenger()->addMessage($this->t('The configuration for the block has been set.'));
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 86400;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContext() {
    return Cache::mergeContexts(parent::getCacheContexts(), [
      'url.path',
      'url.query_args',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $mode = $config['mode'] ?? 'php';
    $theme = $config['theme'] ?? 'dark';
    $datas = [
      'co2' => $this->t("No Result"),
      'cleaner' => FALSE,
    ];

    $build = [
      '#theme' => 'websitecarbon',
      '#mode' => $mode,
      '#thm' => $theme,
      '#attached' => [
        'library' => ['websitecarbon/websitecarbon.' . $mode],
      ],
    ];

    if ($mode == 'php') {
      try {
        $current_uri = $this->requestStack->getCurrentRequest();
        $response = $this->httpClient->get($this->apiUrl . $current_uri->getUri());
        $result = json_decode($response->getBody(), TRUE);
      }
      catch (RequestException $e) {
        $this->loggerFactory->error($e->getMessage());
      }
      if (!empty($result['c'])) {
        $datas['co2'] = $result['c'] . $this->t("g of CO<sub>2</sub>/view");
        $datas['cleaner'] = $this->t("Cleaner than @p% of pages tested", ['@p' => $result['p']]);
      }
      $build['#datas'] = $datas;
    }
    return $build;
  }

}
